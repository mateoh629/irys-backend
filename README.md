## Start the server

These are the steps you must follow in order to get the server running, so the app works.

1. Clone this repository.
2. Move to the project's directory.
3. Run npm i on the console to install required dependencies.
4. On the console, type **npm run dev** and press **Enter**.
5. The server will now be listening on port 8000.