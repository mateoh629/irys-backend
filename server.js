const express = require('express')
const cors = require('cors')
const admin = require('firebase-admin')
const serviceAccount = require('./firebaseKey.json')

const port = 8000
const app = express()

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})

const db = admin.firestore()

app.use(cors())
app.use(express.json())
app.use('/', async (req, res) => {
  const categories = await db.collection('categories').get()
  categories.forEach((doc) => {
    res.status(200).json(doc.data())
  })
})

app.listen(port , () => console.log(`App running on port ${port}`))
